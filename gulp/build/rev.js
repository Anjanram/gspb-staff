import gulp from 'gulp';
import rev from 'gulp-rev-all';
import revdel from 'gulp-rev-delete-original'
import config from "../config";

gulp.task('rev', () => {
    if (!config.development) {
        return gulp.src(`${config.distDir}/**`)
            .pipe(rev.revision({
                dontRenameFile: ['.html', '.jpg', '.png', '.svg', '.gif']
            }))
            .pipe(revdel())
            .pipe(gulp.dest(`${config.distDir}`));
    }
});
