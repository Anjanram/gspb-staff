import gulp from "gulp";
import rename from "gulp-rename";
import source from "vinyl-source-stream";
import browserify from "browserify";
import pugify from "pugify";
import watchify from "watchify";
import minifyify from "minifyify";
import stripify from "stripify";
import notify from "gulp-notify";
import config from "../config";

const entryPoint = `./${config.appDir}/js/main.js`;

gulp.task('browserify', () => {
  const bundler = browserify({
    cache: {},
    packageCache: {},
    fullPaths: false,
    debug: config.development,
    extensions: ['.pug', '.js'],
    entries: entryPoint,
    paths: [
      `${config.appDir}/src`,
      `${config.appDir}/lib`
    ]
  })
  .transform(pugify);

  const bundle = () => {
    const bundleStream = bundler.bundle();

    return bundleStream
      .on('error', notify.onError())
      .pipe(source(entryPoint))
      .pipe(rename('js/main.js'))
      .pipe(gulp.dest(config.distDir));
  };

  if (config.development) {
    watchify(bundler).on('update', bundle);
  }
  else {
    bundler.transform(stripify);
    bundler.plugin(minifyify, { map: false });
  }

  return bundle();
});
