'use strict';

var BaseModel = require('./base'),
    deparam = require('jquery-deparam'),
    $ = require('jquery'),
    _ = require('underscore'),
    App = require('../app'),
    ProblemsConfig;

ProblemsConfig = BaseModel.extend({

    defaults: {
        o: 'problem__expected_answer_dt'
    },

    initialize: function () {
        BaseModel.prototype.initialize.call(this);

        this.set('performer', App.dictionaries.profile.get('organization'));
    },

    parse: function (resp) {
        var json = deparam(resp);

        if (!_.isUndefined(json.status) && (json.status != '')) {
            json.status = parseInt(json.status, 10);
        } else {
            delete json.status;
        }

        if (!_.isUndefined(json.district) && (json.district != '')) {
            json.district = parseInt(json.district, 10);
        } else {
            delete json.district;
        }

        return json;
    },

    getURLParams: function () {
        return $.param(BaseModel.prototype.toJSON.apply(this));
    }

});

module.exports = ProblemsConfig;