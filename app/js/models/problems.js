'use strict';

var BaseModel = require('./base'),
    _ = require('underscore'),
    moment = require('moment'),
    AppConfig = require('../config'),
    ProblemsModel;

ProblemsModel = BaseModel.extend({

    urlRoot: AppConfig.apiPath + '/staff/problems/',

    parse: function (res) {
        if (!res) return {};

        res.sidebar.answer_days = res.answer_days;
        res.sidebar.permissions = res.permissions;
        res.sidebar.reason = res.reason;
        return res.sidebar;
    },

    _getConvertedDate: function (date) {
        return moment(date).format('DD.MM.YYYY')
    },

    getAnswerEnd: function () {
        return this._getConvertedDate(this.get('expected_answer_dt'));
    },

    getPriority: function () {
        return this.get('priority');
    },

    getCoordinates: function () {
        return {
            lat: this.get('latitude'),
            lon: this.get('longitude')
        };
    },

    _getPermission: function (name) {
        return _.findWhere(this.get('permissions'), {name: name});
    },

    _isPermissionActive: function (permission) {
        return !_.isUndefined(permission) ? permission.active : false;
    },

    _checkPermission: function (name) {
        return this._isPermissionActive(this._getPermission(name));
    },

    isCanPerformerAnswerCreate: function () {
        return this._checkPermission('performer.answer.create');
    },

    isCanProblemUserAssign: function () {
        return this._checkPermission('problem.user_assign');
    },

    isCanPerformerRoutesPerformerRefuse: function () {
        return this._checkPermission('performer.routes.performer.refuse');
    },

    getDiff: function () {
        var expected_dt = moment(this.get('expected_answer_dt')),
            now_dt = moment();

        return Math.floor(expected_dt.diff(now_dt, 'days', true));
    }

});

module.exports = ProblemsModel;