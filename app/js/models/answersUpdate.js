'use strict';

var BaseAnswerModel = require('./answers'),
    Bb = require('backbone'),
    _ = require('underscore'),
    AppConfig = require('../config'),
    AnswersUpdateModel;

AnswersUpdateModel = BaseAnswerModel.extend({

    urlRoot: AppConfig.apiPath + '/staff/answers/',

    parse: function (resp) {

        this.set('files', new Bb.Collection());

        var files = this.get('files');

        if (resp.files) {
            _.each(resp.files, function (file) {
                files.add({file: _.extend(file, {type: 'file'})});
            }, this);
        }

        if (resp.photos) {
            _.each(resp.photos, function (photo) {
                files.add({file: _.extend(photo, {type: 'image'})});
            }, this);
        }

        if (resp == "") {
            return {};
        }

        return _.extend(_.pick(resp, 'id', 'problem_id', 'body', 'interim_date'), {is_interim: !_.isNull(resp.interim_date)});
    }

});

module.exports = AnswersUpdateModel;