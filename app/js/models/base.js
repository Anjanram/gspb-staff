'use strict';

var Bb = require('backbone'),
    _ = require('underscore'),
    gspb = require('../services/session'),
    History = require('../services/history'),
    BaseModel;

BaseModel = Bb.Model.extend({

    url: function() {
        var origUrl = Bb.Model.prototype.url.call(this);
        return origUrl + (origUrl.charAt(origUrl.length - 1) == '/' ? '' : '/');
    },

    initialize: function () {
        this.listenTo(this, 'error', function (model, response, options) {
            if (response.status == 401) {
                History.redirectToLogin();
            }
        })
    },

    methodMap: {
        'create': 'POST',
        'update': 'PUT',
        'delete': 'DELETE',
        'read':   'GET'
    },

    sync: function (method, model, options) {
        var type = this.methodMap[method];
        var self = this;

        if ((type == 'DELETE') || (type == 'GET')) {
            return Bb.sync(method, model, options);
        } else {
            var form = new FormData();

            _.each(this.attributes, function( value, key ){
                if (key == 'files') {
                    _.each(value.models, function(file) {
                        if (file.get('file').id) {
                            form.append('old_files', file.get('file').id);
                        } else {
                            form.append(key, file.get('file'));
                        }
                    });
                    return;
                }
                form.append( key, value );
            });

            var url = this.url();

            if (options.url)
                url = options.url;

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4)
                    if (xhr.status== 200 || xhr.status == 201) {
                        options.success(xhr.response,xhr.status,xhr);
                    } else {
                        self.trigger('error', self, JSON.parse(xhr.response) || {}, options);
                    }
            };

            xhr.open(type, url);
            xhr.setRequestHeader('X-CSRFToken', gspb.getCSRFToken());
            xhr.send(form);
            model.trigger('request', model, xhr, options);

            return xhr;
        }

    }

});

module.exports = BaseModel;