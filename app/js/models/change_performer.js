'use strict';

var BaseModel = require('./base'),
    Bb = require('backbone'),
    AppConfig = require('../config'),
    ChangePerformerModel;

ChangePerformerModel =  BaseModel.extend({

    urlRoot: function () {
        return AppConfig.apiPath + '/staff/problems/' + this.get('problem_id') + '/performer_change/';
    },

    initialize: function () {
        if (!this.get('files')) {
            this.set('files', new Bb.Collection());
        }
    }

});

module.exports = ChangePerformerModel;