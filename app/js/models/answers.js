'use strict';

var BaseModel = require('./base'),
    Bb = require('backbone'),
    AppConfig = require('../config'),
    AnswersModel;

AnswersModel = BaseModel.extend({

    defaults: {
        is_interim: false
    },

    urlRoot: function () {
        return AppConfig.apiPath + '/staff/problems/' + this.get('problem_id') + '/answer_create/';
    },

    initialize: function () {
        if (!this.get('files')) {
            this.set('files', new Bb.Collection());
        }
    }

});

module.exports = AnswersModel;