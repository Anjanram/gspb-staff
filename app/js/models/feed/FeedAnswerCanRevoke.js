'use strict';

var BaseFeed = require('./BaseAnswer'),
    AppConfig = require('../../config'),
    FeedAnswerCanRevoke;

FeedAnswerCanRevoke = BaseFeed.extend({

    url: function () {
        return AppConfig.apiPath + '/staff/answers/' + this.id + '/revoke/';
    }

});

module.exports = FeedAnswerCanRevoke;