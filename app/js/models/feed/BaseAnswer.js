'use strict';

var BaseFeed = require('./BaseFeed'),
    _ = require('underscore'),
    moment = require('moment'),
    BaseAnswer;

BaseAnswer = BaseFeed.extend({
    
    getTitle: function () {
        var interim_date = this.getFromPayload('interim_date');

        if (!_.isNull(interim_date)) {
            return 'Промежуточный ответ';
        } else {
            return 'Ответ';
        }
    },

    getInfos: function () {
        var infos = [];

        if (!_.isUndefined(this.getFromPayload('interim_date')) && this.getFromPayload('interim_date')) {
            var date = moment(this.getFromPayload('interim_date'));

            var _date = (date.isValid() ? date.format('DD.MM.YYYY') : ' ');

            infos.push('Дата устранения: ' + _date);
        }

        return infos;
    }
    
});

module.exports = BaseAnswer;