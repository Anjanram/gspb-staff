'use strict';

var BaseFeed = require('./BaseFeed'),
    BaseStatus;

BaseStatus = BaseFeed.extend({
    
    getTitle: function () {
        return this.getFromPayload('status_name');
    },

    getBody: function () {
        return null;
    },

    getStatusName: function () {
        return null;
    }
    
});

module.exports = BaseStatus;