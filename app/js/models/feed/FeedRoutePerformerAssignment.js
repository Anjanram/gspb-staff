'use strict';

var BaseRouteFeed = require('./BaseRoute'),
    moment = require('moment'),
    FeedRoutePerformerAssignment;

FeedRoutePerformerAssignment = BaseRouteFeed.extend({

    getBody: function () {
        //TODO Сделать общий метод конвертирования дат
        var date = moment(this.getFromPayload('response_before_dt'));

        var _date = (date.isValid() ? date.format('DD.MM.YYYY') : ' ');

        var body = this.getFromPayload('comment');

        var _body = (body != '' ? (body + '<br><br>') : '');

        return _body + 'Предоставить ответ до: ' + _date;
    }

});

module.exports = FeedRoutePerformerAssignment;