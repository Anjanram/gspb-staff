'use strict';

var BaseFeed = require('./BaseFeed'),
    BasePetition;

BasePetition = BaseFeed.extend({
    
    getTitle: function () {
        return 'Сообщение №' + this.id;
    }
    
});

module.exports = BasePetition;