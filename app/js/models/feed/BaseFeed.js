'use strict';

var BaseModel = require('../base'),
    Bb = require('backbone'),
    _ = require('underscore'),
    moment = require('moment'),
    Session = require('../../services/session'),
    FeedModel;

FeedModel = BaseModel.extend({

    parse: function (resp) {
        this.id = resp.payload.id;

        return resp;
    },

    sync: function (method, model, options) {
        options.beforeSend = function (xhr, setting) {
            xhr.setRequestHeader('X-CSRFToken', Session.getCSRFToken());
            return xhr;
        };

        return Bb.sync(method, model, options);
    },

    getWidget: function () {
        return this.get('widget');
    },

    getMeta: function () {
        return this.get('meta');
    },

    getFromMeta: function (key) {
        return this.getMeta()[key];
    },

    getPayload: function () {
        return this.get('payload');
    },

    getFromPayload: function (key) {
        return this.getPayload()[key];
    },

    getTitle: function () {
        return this.getFromMeta('title');
    },

    getLabelFontColor: function () {
        return this.getFromMeta('status_font_color');
    },

    getLabelBgColor: function () {
        return this.getFromMeta('status_bg_color');
    },

    getLabelStyle: function () {
        var style = '';

        if (!_.isUndefined(this.getLabelFontColor())) {
            style += ' color: ' + this.getLabelFontColor() + ';';
        }

        if (!_.isUndefined(this.getLabelBgColor())) {
            style += ' background-color: ' + this.getLabelBgColor() + ';';
        }

        return style;
    },

    getRejectBgColor: function () {
        return this.getFromMeta('reject_bg_color');
    },

    getRejectBorderColor: function () {
        return this.getFromMeta('reject_border_color');
    },

    getRejectBgStyle: function () {
        var style = '';

        if (!_.isUndefined(this.getRejectBgColor())) {
            style += ' background-color: ' + this.getRejectBgColor() + ';';
        }

        return style;
    },

    getRejectBorderStyle: function () {
        var style = '';

        if (!_.isUndefined(this.getRejectBorderColor())) {
            style += ' background-color: ' + this.getRejectBorderColor() + ';';
        }

        return style;
    },

    getBorderColor: function () {
        return this.getFromMeta('bg_color');
    },

    getBgColor: function () {
        return this.getFromMeta('border_color');
    },

    getDate: function () {
        var date = moment(this.getFromPayload('dt'));

        return date.isValid() ? date.format('DD.MM.YYYY, HH:mm') : ' ';
    },

    getAuthorName: function () {
        return this.getFromPayload('author_name');
    },

    getBody: function () {
        return this.getFromPayload('body');
    },

    getStatusName: function () {
        return this.getFromPayload('status_name');
    },

    isHaveImages: function () {
        var photos = this.getFromPayload('photos');

        return !_.isUndefined(photos) && (photos.length > 0)
    },
    
    getImages: function () {
        return this.getFromPayload('photos') || [];
    },

    isHaveFiles: function () {
        var files = this.getFromPayload('files');

        return !_.isUndefined(files) && (files.length > 0)
    },

    getFiles: function () {
        return this.getFromPayload('files') || [];
    },

    getRejectDt: function () {
        var date = moment(this.getFromPayload('reject')['dt']);

        return date.isValid() ? date.format('DD.MM.YYYY, HH:mm') : ' ';
    },

    getRejectAuthor: function () {
        return this.getFromPayload('reject')['author_organization'] || this.getFromPayload('reject')['author_name'];
    },

    getRejectBody: function () {
        return this.getFromPayload('reject')['body_html'];
    }

});

module.exports = FeedModel;