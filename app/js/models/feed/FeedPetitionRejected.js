'use strict';

var BaseFeed = require('./BasePetition'),
    FeedPetitionRejected;

FeedPetitionRejected = BaseFeed.extend({
    
    getRejectAuthorOrganization: function () {
        return this.getFromPayload('reject')['author_name'];
    }
    
});

module.exports = FeedPetitionRejected;