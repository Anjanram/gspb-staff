'use strict';

var BaseRouteFeed = require('./BaseRoute'),
    AppConfig = require('../../config'),
    FeedRoutePerformerChangeDraft;

FeedRoutePerformerChangeDraft = BaseRouteFeed.extend({

    isNew: function () {
        return false;
    },

    url: function () {
        return AppConfig.apiPath + '/staff/performer_changes/' + this.id + '/';
    },

    getTitle: function () {
        return 'Запрос на смену исполнителя';
    }

});

module.exports = FeedRoutePerformerChangeDraft;