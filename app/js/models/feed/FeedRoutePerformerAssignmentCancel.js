'use strict';

var BaseRouteFeed = require('./BaseRoute'),
    FeedRoutePerformerAssignmentCancel;

FeedRoutePerformerAssignmentCancel = BaseRouteFeed.extend({

    getTitle: function () {
        return 'Снято с исполнителя';
    },

    getBody: function () {
        return null;
    },

    getInfos: function () {
        var infos = [];

        if (!_.isUndefined(this.getFromPayload('performer')) && !_.isNull(this.getFromPayload('performer'))) {
            infos.push('✖ ' + this.getFromPayload('performer')['name']);
        }

        return infos;
    }

});

module.exports = FeedRoutePerformerAssignmentCancel;