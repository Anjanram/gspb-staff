'use strict';

var BaseRouteFeed = require('./BaseRoute'),
    FeedRoutePerformerAssignment;

FeedRoutePerformerAssignment = BaseRouteFeed.extend({

    getTitle: function () {
        return 'Запрос на смену исполнителя';
    }

});

module.exports = FeedRoutePerformerAssignment;