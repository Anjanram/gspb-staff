'use strict';

var BaseFeed = require('./BaseFeed'),
    _ = require('underscore'),
    moment = require('moment'),
    FeedExecutionDateChange;

FeedExecutionDateChange = BaseFeed.extend({

    getTitle: function () {
        return 'Смена срока исполнения';
    },

    getBody: function () {
        return this.getFromPayload('comment');
    },

    getInfos: function () {
        var infos = [];

        if  (
                (!_.isUndefined(this.getFromPayload('current_execution_dt')) && !_.isNull(this.getFromPayload('current_execution_dt'))) &&
                (!_.isUndefined(this.getFromPayload('new_execution_dt')) && !_.isNull(this.getFromPayload('new_execution_dt')))
            ) {

            var olddate = moment(this.getFromPayload('current_execution_dt'));

            var _olddate = olddate.isValid() ? olddate.format('DD.MM.YYYY') : ' ';

            var newdate = moment(this.getFromPayload('new_execution_dt'));

            var _newdate = newdate.isValid() ? newdate.format('DD.MM.YYYY') : ' ';

            infos.push('Решение до ' + _olddate + ' → ' + _newdate);
        }

        return infos;
    }
    
});

module.exports = FeedExecutionDateChange;