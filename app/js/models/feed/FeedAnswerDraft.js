'use strict';

var BaseFeed = require('./BaseAnswer'),
    AppConfig = require('../../config'),
    FeedAnswerDraft;

FeedAnswerDraft = BaseFeed.extend({

    //TODO fix
    isNew: function () {
        return false;
    },

    url: function () {
        return AppConfig.apiPath + '/staff/answers/' + this.id + '/';
    }

});

module.exports = FeedAnswerDraft;