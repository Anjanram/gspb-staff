'use strict';

var BaseRouteFeed = require('./BaseRoute'),
    AppConfig = require('../../config'),
    FeedRoutePerformerChangeCanRevoke;

FeedRoutePerformerChangeCanRevoke = BaseRouteFeed.extend({

    url: function () {
        return AppConfig.apiPath + '/staff/performer_changes/' + this.id + '/revoke/';
    },

    getTitle: function () {
        return 'Запрос на смену исполнителя';
    }

});

module.exports = FeedRoutePerformerChangeCanRevoke;