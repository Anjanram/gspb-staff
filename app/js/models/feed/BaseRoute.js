'use strict';

var BaseFeed = require('./BaseFeed'),
    _ = require('underscore'),
    moment = require('moment'),
    FeedRoute;

FeedRoute = BaseFeed.extend({

    isAutoRoute: function () {
        return this.getFromPayload('is_autoroute');
    },

    getTitle: function () {
        return this.isAutoRoute() ? 'Автомаршрутизация' : this.getFromMeta('title');
    },

    getBody: function () {
        return this.getFromPayload('comment');
    },

    getInfos: function () {
        var infos = [];

        if (!_.isUndefined(this.getFromPayload('controller')) && !_.isNull(this.getFromPayload('controller'))) {
            infos.push('Контролёр → <br>' + this.getFromPayload('controller')['name']);
        }

        if (!_.isUndefined(this.getFromPayload('coordinator')) && !_.isNull(this.getFromPayload('coordinator'))) {
            infos.push('Координатор → <br>' + this.getFromPayload('coordinator')['name']);
        }

        if (!_.isUndefined(this.getFromPayload('performer')) && !_.isNull(this.getFromPayload('performer'))) {
            infos.push('Исполнитель → <br>' + this.getFromPayload('performer')['name']);
        }

        if  (
                (!_.isUndefined(this.getFromPayload('new_expected_answer_dt')) && !_.isNull(this.getFromPayload('new_expected_answer_dt'))) &&
                (!_.isUndefined(this.getFromPayload('old_expected_answer_dt')) && !_.isNull(this.getFromPayload('old_expected_answer_dt')))
            ) {

            var olddate = moment(this.getFromPayload('old_expected_answer_dt'));

            var _olddate = olddate.isValid() ? olddate.format('DD.MM.YYYY') : ' ';

            var newdate = moment(this.getFromPayload('new_expected_answer_dt'));

            var _newdate = newdate.isValid() ? newdate.format('DD.MM.YYYY') : ' ';

            infos.push('Срок решения изменен с ' + _olddate + ' на ' + _newdate);
        }

        return infos;
    }
    
});

module.exports = FeedRoute;