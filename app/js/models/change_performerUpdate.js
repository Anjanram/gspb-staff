'use strict';

var BaseChangePerformerModel = require('./change_performer'),
    Bb = require('backbone'),
    _ = require('underscore'),
    AppConfig = require('../config'),
    ChangePerformerUpdateModel;

ChangePerformerUpdateModel = BaseChangePerformerModel.extend({

    urlRoot: function () {
        return AppConfig.apiPath + '/staff/performer_changes/';
    },

    parse: function (resp) {
        this.set('files', new Bb.Collection());

        var files = this.get('files');

        if (resp.files) {
            _.each(resp.files, function (file) {
                files.add({file: _.extend(file, {type: 'file'})});
            }, this);
        }

        if (resp.photos) {
            _.each(resp.photos, function (photo) {
                files.add({file: _.extend(photo, {type: 'image'})});
            }, this);
        }

        return _.extend(_.pick(resp, 'id', 'problem_id', 'comment', 'performer'));
    }

});

module.exports = ChangePerformerUpdateModel;