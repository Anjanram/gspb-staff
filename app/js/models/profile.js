'use strict';

var BaseModel =  require('./base'),
    AppConfig = require('../config'),
    History = require('../services/history'),
    ProfileModel;

ProfileModel = BaseModel.extend({

    urlRoot: AppConfig.apiPath + '/accounts/profile/',

    initialize: function () {
        BaseModel.prototype.initialize.call(this);

        this.listenToOnce(this, 'sync', function (model, response, options) {
            if (model.get('role') != 3) {
                History.redirectToLogin();
            }
        });
    }

});

module.exports = ProfileModel;