'use strict';

var Bb = require('backbone'),
    moment = require('moment'),
    BaseProblemsListItem;

BaseProblemsListItem = Bb.Model.extend({

    _getConvertedDate: function (date) {
        return moment(date).format('DD.MM.YYYY  HH:mm')
    },

    getAnswerStart: function () {
        return this._getConvertedDate(this.get('assign_dt'))
    },

    getAnswerEnd: function () {
        return this._getConvertedDate(this.get('expected_answer_dt'))
    },

    getAnswerLast: function () {
        return this._getConvertedDate(this.get('last_petition_created_at'))
    },

    getDiff: function () {
        var expected_dt = moment(this.get('expected_answer_dt')),
            now_dt = moment();

        return Math.floor(expected_dt.diff(now_dt, 'days', true));
    },

    getPriority: function () {
        return this.get('priority')
    }

});

module.exports = BaseProblemsListItem;