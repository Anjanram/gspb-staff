'use strict';

var App = require('../app'),
    NotificationView = require('../views/components/notification');

App.on('before:start', function () {
    App.layout.showChildView('notificationsRegion',
        new NotificationView()
    );
});
