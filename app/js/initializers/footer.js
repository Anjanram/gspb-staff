'use strict';

var App = require('../app'),
    FooterController = require('../controllers/footer');

App.on('before:start', function() {
    new FooterController();

    console.log('footer start');
});
