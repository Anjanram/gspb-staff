'use strict';

var App = require('../app'),
    Router = require('../routers/problem'),
    routes = require('../helpers/routes');

App.on('before:start', function() {
    var router = new Router();
    routes.bind('problem', router);
    console.log('module executor started');
});
