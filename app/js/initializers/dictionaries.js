'use strict';

var $ = require('jquery'),
    app = require('../app'),
    OrgsCollection = require('../collections/dictionaries/organizations'),
    StatusesCollection = require('../collections/dictionaries/statuses'),
    DistrictsCollection = require('../collections/dictionaries/district'),
    ProfileModel = require('../models/profile');

app.dictionaries = {};
app.dictionaries.organizations = new OrgsCollection();
app.dictionaries.profile = new ProfileModel();
app.dictionaries.statuses = new StatusesCollection();
app.dictionaries.districts = new DistrictsCollection();

$.when(
    app.dictionaries.organizations.fetch(),
    app.dictionaries.profile.fetch(),
    app.dictionaries.statuses.fetch(),
    app.dictionaries.districts.fetch()
).then(function () {
    app.start();

    console.log('dictionaries loaded');
});