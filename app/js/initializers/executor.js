'use strict';

var App = require('../app'),
    Router = require('../routers/executor'),
    routes = require('../helpers/routes');

App.on('before:start', function() {
    var router = new Router();
    routes.bind('executor', router);
    console.log('module executor started');
});
