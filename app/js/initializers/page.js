'use strict';

var App = require('../app'),
    PageController = require('../controllers/page');

App.on('before:start', function() {
    new PageController();

    console.log('footer start');
});
