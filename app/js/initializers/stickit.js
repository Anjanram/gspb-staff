'use strict';

var Bb = require('backbone');

require('backbone.stickit');

Bb.Stickit.addHandler({

    selector: '.control-sort',

    update: function ($el, val, model, options) {
        if ($el.attr('name') == val) {
            $el.addClass('control-sort-plus');
            $el.removeClass('control-sort-minus');
        } else if (('-' + $el.attr('name')) == val) {
            $el.addClass('control-sort-minus');
            $el.removeClass('control-sort-plus');
        } else {
            $el.removeClass('control-sort-plus');
            $el.removeClass('control-sort-minus');
            val = 0;
        }

        $el.val(val);
    }

});