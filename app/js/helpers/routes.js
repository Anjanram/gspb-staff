'use strict';

var MnRoutesHelper = require('marionette-routes-helper');

MnRoutesHelper.initialize({ root: '#/' });

module.exports = MnRoutesHelper;