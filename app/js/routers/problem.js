'use strict';

var BaseRouter = require('./base'),
    ProblemController = require('../controllers/problem'),
    ProblemRouter;

ProblemRouter = BaseRouter.extend({

    appRoutes: {
        'staff/cabinet/problems/:id/': 'index',

        'staff/cabinet/problems/:id/feed/': 'feed',
        'staff/cabinet/problems/:id/feed/user/': 'feedUser',

        'staff/cabinet/problems/:id/answer_create/': 'answer',
        'staff/cabinet/problems/:id/answer_edit/:msg_id/': 'answerEdit',

        'staff/problems/:id/performer_change/': 'performerChange',
        'staff/problems/:id/performer_change/:msg_id/': 'performerChangeEdit'
    },

    appSiteRoutes: {
        'feed': 'staff/cabinet/problems/:id/',
        'feedUser': 'staff/cabinet/problems/:id/',

        'answer': 'staff/cabinet/problems/:id/',
        'answerEdit': 'staff/cabinet/problems/:id/',

        'performerChange': 'staff/cabinet/problems/:id/',
        'performerChangeEdit': 'staff/cabinet/problems/:id/'
    },

    controller: new ProblemController()

});

module.exports = ProblemRouter;