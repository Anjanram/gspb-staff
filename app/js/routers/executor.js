'use strict';

var BaseRouter = require('./base'),
    ExecutorController = require('../controllers/executor'),
    ExecutorRouter;

ExecutorRouter = BaseRouter.extend({

    appRoutes: {
        '': 'index',
        '#': 'index',
        '#/': 'index',
        'staff/cabinet/executor/': 'index',

        'staff/cabinet/executor/problems_answer_preparing/my/?:params': 'my',
        'staff/cabinet/executor/problems_answer_preparing/my/filter/?:params': 'myFilter',
        'staff/cabinet/executor/problems_answer_preparing/my/sort/?:params': 'mySort',

        'staff/cabinet/executor/problems_answer_preparing/organization/?:params': 'organization',
        'staff/cabinet/executor/problems_answer_preparing/organization/filter/?:params': 'organizationFilter',
        'staff/cabinet/executor/problems_answer_preparing/organization/sort/?:params': 'organizationSort',

        'staff/cabinet/executor/problems_answer_preparing/department/?:params': 'department',
        'staff/cabinet/executor/problems_answer_preparing/department/filter/?:params': 'departmentFilter',
        'staff/cabinet/executor/problems_answer_preparing/department/sort/?:params': 'departmentSort',

        'staff/cabinet/executor/problems_answer_drafts/?:params': 'draft',
        'staff/cabinet/executor/problems_answer_drafts/sort/?:params': 'draftSort',
        'staff/cabinet/executor/problems_answer_drafts/filter/?:params': 'draftFilter',

        'staff/cabinet/executor/performer_change_drafts/?:params': 'draftChange',
        'staff/cabinet/executor/performer_change_drafts/sort/?:params': 'draftChangeSort',
        'staff/cabinet/executor/performer_change_drafts/filter/?:params': 'draftChangeFilter'
    },

    appSiteRoutes: {
        'index': 'staff/cabinet/executor/',

        'my': 'staff/cabinet/executor/problems_answer_preparing/my/?:params',
        'myFilter': 'staff/cabinet/executor/problems_answer_preparing/my/?:params',
        'mySort': 'staff/cabinet/executor/problems_answer_preparing/my/?:params',

        'organization': 'staff/cabinet/executor/problems_answer_preparing/organization/?:params',
        'organizationFilter': 'staff/cabinet/executor/problems_answer_preparing/organization/?:params',
        'organizationSort': 'staff/cabinet/executor/problems_answer_preparing/organization/?:params',

        'department': 'staff/cabinet/executor/problems_answer_preparing/department/?:params',
        'departmentFilter': 'staff/cabinet/executor/problems_answer_preparing/department/filter/?:params',
        'departmentSort': 'staff/cabinet/executor/problems_answer_preparing/department/sort/?:params',

        'draft': 'staff/cabinet/executor/problems_answer_draft/?:params',
        'draftSort': 'staff/cabinet/executor/problems_answer_draft/?:params',
        'draftFilter': 'staff/cabinet/executor/problems_answer_draft/?:params',

        'draftChange': 'staff/cabinet/executor/problems_performer_change_draft/?:params',
        'draftChangeSort': 'staff/cabinet/executor/problems_performer_change_draft/?:params',
        'draftChangeFilter': 'staff/cabinet/executor/problems_performer_change_draft/?:params'
    },

    controller: new ExecutorController()

});

module.exports = ExecutorRouter;


