'use strict';

var Mn = require('backbone.marionette'),
    $ = require('jquery'),
    Events = require('../services/events'),
    Config = require('../config'),
    App = require('../app'),
    PageLoader = require('../views/components/PageLoader'),
    BaseRouter;

require('backbone.routefilter');

BaseRouter = Mn.AppRouter.extend({

    before: function (route) {
        var $header = $('#header_region');

        var elementHeight = $header.height();

        $('html, body').animate({
            scrollTop: elementHeight
        }, 500);

        App.layout.showChildView('pageRegion', new PageLoader());
    },

    onRoute: function (name, path, args) {
        var pattern = path;

        if (this.appSiteRoutes[name]) {
            pattern = this.appSiteRoutes[name];
        }

        var keys = pattern.match(/\:\w+/g);

        var getSiteUrl = function () {
            for (var _len = arguments.length, params = Array(_len), _key = 0; _key < _len; _key++) {
                params[_key] = arguments[_key];
            }

            var path = pattern;

            if (!keys) return Config.rootPath + path;

            if (keys.length !== params.length) {
                throw new Error('incorrect params count (' + params.length + ' for ' + keys.length + ')');
            }

            params.forEach(function (param) {
                path = path.replace(/\:\w+/, param);
            });

            return  Config.rootPath + path;
        };

        var _path = getSiteUrl(args.splice(0, 1));

        Events.trigger('route:change', {
            path: _path
        });
    }

});

module.exports = BaseRouter;