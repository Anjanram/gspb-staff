'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    FormBehavior = require('../../behaviors/form'),
    FileBehavior = require('../../behaviors/file'),
    Events = require('../../services/events'),
    AnswerView;

AnswerView = Mn.View.extend({

    template: require('./tpl/layout.pug'),

    ui: {
        btn_send: '.js-send',
        btn_draft: '.js-draft'
    },

    events: {
        'click @ui.btn_send': 'onSubmit',
        'click @ui.btn_draft': 'onClickSaveDraft'
    },

    bindings: {
        '[name="body"]': {
            observe: 'body',
            updateView: true
        },
        '[name="is_interim"]': {
            observe: 'is_interim',
            updateView: true,
            onSet: function (val, options) {
                return (val == 'true') || (val == true);
            }
        },
        '[name="interim_date"]': {
            observe: 'interim_date',
            updateView: true,
            initialize: function ($el, model, options) {
                var old_interim_date = model.get('interim_date');

                function checkIsInterim() {
                    if (model.get('is_interim')) {
                        $el.parent().show();
                        model.set('interim_date', old_interim_date)
                    } else {
                        $el.parent().hide();
                        model.set('interim_date', '');
                    }
                }

                this.listenTo(model, 'change:is_interim', checkIsInterim);

                checkIsInterim();
            }
        }
    },

    behaviors: {
        form: {
            behaviorClass: FormBehavior
        },
        file: {
            behaviorClass: FileBehavior
        }
    },

    onSubmit: function (e) {
        e.preventDefault();

        var problem_id = this.model.get('problem_id');

        var _this = this;

        this.model.unset('draft');

        this.model.save(null, {
            type: 'POST',
            success: function () {
                Events.trigger('notification:success', {
                    message: 'Ответ ' + (_this.model.get('id') ? 'обновлен' : 'создан')
                });

                window.location.href = Routes.problemFeedPath(problem_id);
            }
        });
    },

    onClickSaveDraft: function (e) {
        e.preventDefault();

        var problem_id = this.model.get('problem_id');

        var _this = this;

        if (this.model.get('draft')) {
            this.model.set('draft', false);
        } else {
            this.model.set('draft', true);
        }

        this.model.save(null, {
            type: 'POST',
            success: function () {
                Events.trigger('notification:success', {
                    message: 'Черновик ответа ' + (_this.model.get('id') ? 'обновлен' : 'создан')
                });

                window.location.href = Routes.problemFeedPath(problem_id);
            }
        });
    }

});

module.exports = AnswerView;