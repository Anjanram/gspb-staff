'use strict';

var Mn = require('backbone.marionette'),
    FeedBtnItem = require('./FeedBtn'),
    FeedBtnsCollection;

FeedBtnsCollection = Mn.CollectionView.extend({

    className: 'btn-group',

    childView: FeedBtnItem

});

module.exports = FeedBtnsCollection;