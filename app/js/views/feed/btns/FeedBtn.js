'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    FeedBtn;

FeedBtn = Mn.View.extend({

    template: _.template('<%= title%>'),

    className: 'btn',
    
    events: {
        'click': 'onClick'
    },

    onRender: function () {
        this.$el.addClass(this.model.get('class'));
    },
    
    onClick: function () {
        debugger;
        this.triggerMethod('btn:click:' + this.model.get('onClick'), this);
    }

});

module.exports = FeedBtn;