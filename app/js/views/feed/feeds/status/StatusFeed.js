'use strict';

var BaseFeed = require('../BaseFeed'),
    StatusFeed;

StatusFeed = BaseFeed.extend({

    template: require('./StatusFeed.pug'),

    regions: {

    },

    behaviors: {

    }

});

module.exports = StatusFeed;