'use strict';

var BaseFeed = require('../BaseFeed'),
    Bb = require('backbone'),
    Events = require('../../../../services/events'),
    History = require('../../../../services/history'),
    Routes = require('../../../../helpers/routes'),
    FeedBtnsCollectionView = require('../../btns/FeedBtnsCollection'),
    AppConfig =require('../../../../config'),
    AnswerCanRevoke;

AnswerCanRevoke = BaseFeed.extend({

    template: require('./AnswerDraft.pug'),

    childViewEvents: {
        'btn:click:delete': function () {
            var is_delete_confirmed = confirm('Удалить черновик?');

            if (is_delete_confirmed) {
                this.model.destroy();

                Events.trigger('notification:success', {
                   message: 'Черновик удален'
                });
            }
        },
        'btn:click:edit': function () {
            var payload = this.model.get('payload');

            var editModel = new Bb.Model({
                body: payload.body,
                interim_date: payload.interim_date
            });

            editModel.urlRoot = AppConfig.apiPath + '/staff/problems/' + this.model.collection.id + '/answer_create/';

            var href = Routes.problemAnswerEditPath(this.model.collection.id, this.model.id);

            History.navigate(href);
        }
    },

    onRender: function () {
        var btn = new Bb.Model({
            title: 'Удалить',
            type: 'btn',
            class: 'btn-default',
            onClick: 'delete'
        });

        var btn2 = new Bb.Model({
            title: 'Редактировать',
            type: 'link',
            class: 'btn-primary',
            onClick: 'edit'
        });

        var btns = new Bb.Collection([btn, btn2]);

        this.showChildView('btnsRegion', new FeedBtnsCollectionView({
            collection: btns
        }));
    }

});

module.exports = AnswerCanRevoke;