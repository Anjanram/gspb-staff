'use strict';

var BaseFeed = require('../BaseFeed'), 
    PetitionFeed;

PetitionFeed = BaseFeed.extend({

    template: require('./PetitionFeed.pug')

});

module.exports = PetitionFeed;