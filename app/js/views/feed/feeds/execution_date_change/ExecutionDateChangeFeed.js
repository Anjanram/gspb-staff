'use strict';

var BaseFeed = require('../BaseFeed'),
    RouteFeed;

RouteFeed = BaseFeed.extend({

    template: require('./ExecutionDateChangeFeed.pug'),

});

module.exports = RouteFeed;