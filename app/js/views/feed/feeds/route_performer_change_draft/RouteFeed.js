'use strict';

var BaseFeed = require('../BaseFeed'),
    Bb = require('backbone'),
    History = require('../../../../services/history'),
    Routes = require('../../../../helpers/routes'),
    FeedBtnsCollectionView = require('../../btns/FeedBtnsCollection'),
    RouteFeed;

RouteFeed = BaseFeed.extend({

    template: require('./RouteFeed.pug'),

    childViewEvents: {
        'btn:click:delete': function () {
            var is_delete_confirmed = confirm('Удалить черновик?');

            if (is_delete_confirmed) {
                this.model.destroy();
            }
        },
        'btn:click:edit': function () {
            var href = Routes.problemPerformerChangeEditPath(this.model.collection.id, this.model.id);

            History.navigate(href);
        }
    },

    onRender: function () {
        var btn = new Bb.Model({
            title: 'Удалить',
            type: 'btn',
            class: 'btn-default',
            onClick: 'delete'
        });

        var btn2 = new Bb.Model({
            title: 'Редактировать',
            type: 'link',
            class: 'btn-primary',
            onClick: 'edit'
        });

        var btns = new Bb.Collection([btn, btn2]);

        this.showChildView('btnsRegion', new FeedBtnsCollectionView({
            collection: btns
        }));
    }

});

module.exports = RouteFeed;