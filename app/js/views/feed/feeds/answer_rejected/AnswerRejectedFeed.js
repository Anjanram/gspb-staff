'use strict';

var BaseFeed = require('../BaseFeed'),
    AnswerRejectedFeed;

AnswerRejectedFeed = BaseFeed.extend({

    template: require('./AnswerRejectedFeed.pug')

});

module.exports = AnswerRejectedFeed;