'use strict';

var BaseFeed = require('../BaseFeed'),
    RouteFeed;

RouteFeed = BaseFeed.extend({

    template: require('./RouteFeed.pug')

});

module.exports = RouteFeed;