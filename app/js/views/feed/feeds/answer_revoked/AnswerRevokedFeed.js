'use strict';

var BaseFeed = require('../BaseFeed'),
    AnswerRevokedFeed;

AnswerRevokedFeed = BaseFeed.extend({

    template: require('./AnswerRevokedFeed.pug')

});

module.exports = AnswerRevokedFeed;