'use strict';

var BaseFeed = require('../BaseFeed'), 
    PetitionRejected;

PetitionRejected = BaseFeed.extend({

    template: require('./PetitionRejectedFeed.pug')

});

module.exports = PetitionRejected;