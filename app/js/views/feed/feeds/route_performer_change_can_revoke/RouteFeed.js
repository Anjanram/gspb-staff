'use strict';

var BaseFeed = require('../BaseFeed'),
    Bb = require('backbone'),
    FeedBtnsCollectionView = require('../../btns/FeedBtnsCollection'),
    RouteFeed;

RouteFeed = BaseFeed.extend({

    template: require('./RouteFeed.pug'),

    childViewEvents: {
        'btn:click:revoke': function () {
            var _this = this;

            this.model.save();

            //TODO rewrite
            setTimeout(function () {
                _this.model.collection.fetch({reset: true});
            }, 500);
        }
    },

    onRender: function () {
        var btn = new Bb.Model({
            title: 'Отозвать',
            type: 'btn',
            class: 'btn-primary',
            onClick: 'revoke'
        });

        var btns = new Bb.Collection([btn]);

        this.showChildView('btnsRegion', new FeedBtnsCollectionView({
            collection: btns
        }));
    }

});

module.exports = RouteFeed;