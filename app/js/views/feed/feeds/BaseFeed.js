'use strict';

var Mn = require('backbone.marionette'),
    PhotosBehaviors = require('../../../behaviors/feed/photos/photo'),
    FilesBehaviors = require('../../../behaviors/feed/files/file'),
    BaseFeed;

BaseFeed = Mn.View.extend({

    className: 'feed-wrapper',

    regions: {
        photosRegion: '.feed__gallery',
        filesRegion: '.feed__gallery-files',
        btnsRegion: '.feed__btns'
    },

    behaviors: {
        photos: {
            behaviorClass: PhotosBehaviors
        },
        files: {
            behaviorClass: FilesBehaviors
        }
    },

    template: require('./BaseFeed.pug'),

    templateContext: function () {
        var _this = this;

        return {
            getTitle: function () {
                return _this.model.getTitle();
            },

            getDate: function () {
               return _this.model.getDate();
            },

            getLabelStyle: function () {
                return _this.model.getLabelStyle();
            },

            getBody: function () {
                return _this.model.getBody();
            },

            getAuthor: function () {
                return _this.model.getAuthorName();
            },

            getStatusName: function () {
                return _this.model.getStatusName();
            },

            getInfos: function () {
                return _this.model.getInfos();
            },

            isAutoRoute: function () {
                return _this.model.isAutoRoute();
            },

            getRejectBgStyle: function () {
                return _this.model.getRejectBgStyle();
            },

            getRejectBorderStyle: function () {
                return _this.model.getRejectBorderStyle();
            },

            getRejectDt: function () {
                return _this.model.getRejectDt();
            },

            getRejectAuthor: function () {
                return _this.model.getRejectAuthor();
            },

            getRejectBody: function () {
                return _this.model.getRejectBody();
            }
        }
    }

});

module.exports = BaseFeed;