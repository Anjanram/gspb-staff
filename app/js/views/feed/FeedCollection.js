'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    BaseFeed = require('./feeds/BaseFeed'),
    PetitionFeed = require('./feeds/petition/PetitionFeed'),
    PetitionRejectedFeed = require('./feeds/petition_rejected/PetitionRejectedFeed'),
    StatusFeed = require('./feeds/status/StatusFeed'),
    RouteFeed = require('./feeds/route/RouteFeed'),
    RouteRejectedFeed = require('./feeds/route_rejected/RouteFeed'),
    RoutePerformerAssignmentFeed = require('./feeds/route_performer_assignment/RouteFeed'),
    RoutePerformerAssignmentCancelFeed = require('./feeds/route_performer_assignment_cancel/RouteFeed'),
    RoutePerformerChangeRejectedFeed = require('./feeds/route_performer_change_rejected/RouteFeed'),
    RoutePerformerChangeCanRevoke = require('./feeds/route_performer_change_can_revoke/RouteFeed'),
    RoutePerformerChangeDraft = require('./feeds/route_performer_change_draft/RouteFeed'),
    AnswerDraft = require('./feeds/answer_draft/AnswerDraft'),
    AnswerRevokedFeed = require('./feeds/answer_revoked/AnswerRevokedFeed'),
    AnswerRejectedFeed = require('./feeds/answer_rejected/AnswerRejectedFeed'),
    AnswerCanRevokeFeed = require('./feeds/answer_can_revoke/AnswerCanRevoke'),
    ExecutionDateChangeFeed = require('./feeds/execution_date_change/ExecutionDateChangeFeed'),
    FeedCollection;

FeedCollection = Mn.CollectionView.extend({

    className: 'feed-list',

    childView: function (model) {
        //TODO сделать общую логику определения с коолекцией
        var feed_type = model.get('widget');

        var view;

        switch (feed_type) {
            case 'petition':
                view = PetitionFeed;
                break;

            case 'petition_rejected':
                view = PetitionRejectedFeed;
                break;

            case 'status':
                view = StatusFeed;
                break;

            case 'route':
                view = RouteFeed;
                break;

            case 'route_rejected':
                view = RouteRejectedFeed;
                break;

            case 'route_performer_assignment':
                view = RoutePerformerAssignmentFeed;
                break;

            case 'route_performer_assignment_cancel':
                view = RoutePerformerAssignmentCancelFeed;
                break;

            case 'route_performer_change_revoked':
                view = RoutePerformerAssignmentFeed;
                break;

            case 'route_performer_change_rejected':
                view = RoutePerformerChangeRejectedFeed;
                break;

            case 'route_performer_change_can_revoke':
                view = RoutePerformerChangeCanRevoke;
                break;

            case 'route_performer_change_draft':
                view = RoutePerformerChangeDraft;
                break;

            case 'execution_date_change':
                view = ExecutionDateChangeFeed;
                break;

            case 'answer':
            case 'answer_coordinator_accepted':
                view = AnswerRevokedFeed;
                break;

            case 'answer_draft':
                view = AnswerDraft;
                break;

            case 'answer_revoked':
                view = AnswerRevokedFeed;
                break;

            case 'answer_rejected':
                view = AnswerRejectedFeed;
                break;

            case 'answer_can_revoke':
                view = AnswerCanRevokeFeed;
                break;

            default:
                view = BaseFeed;
        }

        return view;
    }

});

module.exports = FeedCollection;