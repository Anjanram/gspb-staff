'use strict';

var BasePage = require('../page/page'),
    PageHead = require('../page/head'),
    MenuList = require('../menu/list'),
    Bb = require('backbone'),
    app = require('../../app'),
    menu;

menu = BasePage.extend({

    initialize: function (options) {
        this.org = app.dictionaries.profile.get('organization_name');
    },

    settings: {
        title: 'Кабинет исполнителя',
        org: this.org
    },

    showHeader: function () {
        var headModel = new Bb.Model();

        headModel.set('org', this.org);

        this.showChildView('pageRegionHeader',
            new PageHead({
                settings: this.settings,
                model: headModel
            })
        );
    },

    showContent: function () {
        this.showChildView('pageRegionContent',
            new MenuList({
                collection: this.collection,
                settings: this.settings
            })
        )
    },

});

module.exports = menu;