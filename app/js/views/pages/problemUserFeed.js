'use strict';

var BasePage = require('../page/page'),
    Bb = require('backbone'),
    FeedCollectionView = require('../feed/FeedCollection'),
    PageHead = require('../page/head'),
    Routes = require('../../helpers/routes'),
    HeaderBtnsBehaviors = require('../../behaviors/header/base'),
    problemUserFeed;

problemUserFeed = BasePage.extend({

    settings: {
        title: 'Сообщения',
        searchShow: false
    },

    showHeader: function () {
        var btnsCollection = new Bb.Collection([
            {
                href: Routes.problemFeedPath(this.collection.id),
                icon: 'comment'
            },
            {
                href: Routes.problemIndexPath(this.collection.id),
                icon: 'arrow_back'
            }
        ]);

        this.settings.title = 'Сообщения <br>' + 'Проблема №' + this.collection.id;

        this.showChildView('pageRegionHeader', new PageHead({
            settings: this.settings,
            behaviors: [
                {
                    behaviorClass: HeaderBtnsBehaviors,
                    collection: btnsCollection
                }
            ]
        }));
    },

    showContent: function () {
        var view = new FeedCollectionView({
            collection: this.collection,
            settings: this.settings
        });

        view.setFilter(function (child, index, collection) {
            return child.getWidget() == 'petition';
        });

        this.showChildView('pageRegionContent',
            view
        );
    }

});

module.exports = problemUserFeed;