'use strict';

var BasePage = require('../page/page'),
    Bb = require('backbone'),
    FeedCollectionView = require('../feed/FeedCollection'),
    PageHead = require('../page/head'),
    Routes = require('../../helpers/routes'),
    HeaderBtnsBehaviors = require('../../behaviors/header/base'),
    ProblemFeed;

ProblemFeed = BasePage.extend({

    settings: {
        title: 'Лента событий',
        searchShow: false
    },

    showHeader: function () {
        var btnsCollection = new Bb.Collection([
            {
                href: Routes.problemFeedUserPath(this.collection.id),
                icon: 'mail_outline'
            },
            {
                href: Routes.problemIndexPath(this.collection.id),
                icon: 'arrow_back'
            }
        ]);

        this.settings.title = 'Лента событий <br>' + 'Проблема №' + this.collection.id;

        this.showChildView('pageRegionHeader', new PageHead({
            settings: this.settings,
            behaviors: [
                {
                    behaviorClass: HeaderBtnsBehaviors,
                    collection: btnsCollection
                }
            ]
        }));
    },

    showContent: function () {
        this.showChildView('pageRegionContent',
            new FeedCollectionView({
                collection: this.collection,
                settings: this.settings
            })
        );
    }

});

module.exports = ProblemFeed;