'use strict';

var BasePage = require('../page/page'),
    ChangePerformer = require('../change_ performer/change_performer'),
    change_performer;

change_performer = BasePage.extend({

    settings: {
        title: 'Запрос на смену исполнителя',
        titleSearchField: 'Подготовить ответ',
        searchShow: false
    },

    showHeader: function () {
        this.settings.title = 'Запрос на смену исполнителя <br>' + 'Проблема №' + this.model.get('problem_id');

        BasePage.prototype.showHeader.call(this);
    },

    showContent: function () {
        this.showChildView('pageRegionContent',
            new ChangePerformer({
                model: this.model,
                settings: this.settings
            })
        )
    }

});

module.exports = change_performer;