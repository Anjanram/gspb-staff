'use strict';

var BasePageView = require('../page/page'),
    Bb = require('backbone'),
    PageHead = require('../page/head'),
    ProblemView = require('../problem/problem'),
    Routes = require('../../helpers/routes'),
    HeaderBtnsBehaviors = require('../../behaviors/header/base'),
    ProblemPageView;

ProblemPageView = BasePageView.extend({

    settings: {
        title: '',
        searchShow: false
    },

    initialize: function () {
        this.settings.title = 'Проблема №' + this.model.id;
    },

    showHeader: function () {
        var btnsCollection = new Bb.Collection([
            {
                href: Routes.problemFeedUserPath(this.model.id),
                icon: 'mail_outline'
            },
            {
                href: Routes.problemFeedPath(this.model.id),
                icon: 'comment'
            }
        ]);

        this.showChildView('pageRegionHeader', new PageHead({
            settings: this.settings,
            behaviors: [
                {
                    behaviorClass: HeaderBtnsBehaviors,
                    collection: btnsCollection
                }
            ]
        }));
    },

    showContent: function () {
        this.showChildView('pageRegionContent',
            new ProblemView({
                model: this.model,
                settings: this.settings
            })
        );
    }

});

module.exports = ProblemPageView;