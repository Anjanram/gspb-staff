'use strict';

var BasePage = require('../..//page/page'),
    Bb = require('backbone'),
    PageHead = require('../../page/head'),
    Routes = require('../../../helpers/routes'),
    HeaderBtnsBehaviors = require('../../../behaviors/header/base'),
    ProblemsCollectionView = require('../../problems_list/list'),
    BaseProblemsPage;

BaseProblemsPage = BasePage.extend({

    settings: {
        title: '',
        searchShow: true
    },

    showHeader: function () {
        var btnsCollection = new Bb.Collection([
            {
                href: Routes['executor' + this.getOption('type') + 'SortPath'](this.model.getURLParams()),
                icon: 'sort'
            },
            {
                href: Routes['executor' + this.getOption('type') + 'FilterPath'](this.model.getURLParams()),
                icon: 'network_wifi'
            }
        ]);

        this.settings.type = this.getOption('type');
        this.settings.title = this.getOption('title');
        this.settings.titleSearchField = this.getOption('titleSearchField');

        this.showChildView('pageRegionHeader', new PageHead({
            settings: this.settings,
            model: this.model,
            behaviors: [
                {
                    behaviorClass: HeaderBtnsBehaviors,
                    collection: btnsCollection
                }
            ]
        }));
    },

    showContent: function () {
        this.showChildView('pageRegionContent', new ProblemsCollectionView({
            collection: this.collection,
            settings: this.settings
        }));
    }

});

module.exports = BaseProblemsPage;