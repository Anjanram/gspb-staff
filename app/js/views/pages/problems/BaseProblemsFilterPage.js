'use strict';

var BasePageView = require('../../page/page'),
    Bb = require('backbone'),
    PageHead = require('../../page/head'),
    Routes = require('../../../helpers/routes'),
    HeaderBtnsBehaviors = require('../../../behaviors/header/base'),
    FilterView = require('../../filter/Filter'),
    BaseProblemsFilterPage;

BaseProblemsFilterPage = BasePageView.extend({

    settings : {
        title: '',
        searchShow: false
    },

    showHeader: function () {
        var btnsCollection = new Bb.Collection([
            {
                href: Routes['executor' + this.getOption('type') + 'SortPath'](this.model.getURLParams()),
                icon: 'sort'
            },
            {
                href: Routes['executor' + this.getOption('type') + 'FilterPath'](this.model.getURLParams()),
                icon: 'network_wifi',
                active: true
            }
        ]);

        this.settings.title = this.getOption('title');

        this.showChildView('pageRegionHeader', new PageHead({
            settings: this.settings,
            behaviors: [
                {
                    behaviorClass: HeaderBtnsBehaviors,
                    collection: btnsCollection
                }
            ]
        }));
    },

    showContent: function () {
        this.showChildView('pageRegionContent', new FilterView({
            settings: this.settings,
            model: this.model,
            type: this.getOption('type'),
            title: this.getOption('title')
        }));
    }

});

module.exports = BaseProblemsFilterPage;