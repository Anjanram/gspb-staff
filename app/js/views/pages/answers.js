'use strict';

var BasePage = require('../page/page'),
    AnswersView = require('../answers/answer'),
    Answers;

Answers = BasePage.extend({

    settings: {
        title: 'Написать ответ',
        titleSearchField: 'Подготовить ответ',
        searchShow: false
    },

    showHeader: function () {
        this.settings.title = 'Сообщения <br>' + 'Проблема №' + this.model.get('problem_id');

        BasePage.prototype.showHeader.call(this);
    },

    showContent: function () {
        this.showChildView('pageRegionContent',
            new AnswersView({
                model: this.model,
                settings: this.settings
            })
        )
    }
});

module.exports = Answers;