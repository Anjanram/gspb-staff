'use strict';

var Mn = require('backbone.marionette'),
    app = require('../../app'),
    _ = require('underscore'),
    Events = require('../../services/events'),
    selectize = require('selectize'),
    FormBehavior = require('../../behaviors/form'),
    FileBehavior = require('../../behaviors/file'),
    Routes = require('../../helpers/routes'),
    ChangePerformerView;

ChangePerformerView = Mn.View.extend({

    template: require('./tpl/layout.pug'),

    ui: {
        btn_send: '.js-send',
        btn_draft: '.js-draft'
    },

    events: {
        'click @ui.btn_send': 'onSubmit',
        'click @ui.btn_draft': 'onClickSaveDraft'
    },

    bindings: {
        '[name="comment"]': {
            observe: 'comment',
            updateView: true
        },
        '[name="performer"]': {
            observe: 'performer',
            updateView: true,
            updateModel: false,
            initialize: function ($el, model, options) {
                $el.selectize({
                    valueField: 'id',
                    labelField: 'name',
                    searchField: 'name',
                    load: function(query, callback) {
                        if (!query.length) return callback();
                        app.dictionaries.organizations.fetch({
                            data: {
                                q: query
                            },
                            success: function (collection, response, options) {
                                callback(response.slice(0, 20));
                            },
                            error: function () {
                                callback();
                            }
                        })
                    },
                    onChange: function (val) {
                        var new_val = {};
                        new_val[options.observe] = val;

                        model.set(new_val);
                    }
                });
            },
            selectOptions: {
                collection:  app.dictionaries.organizations,
                labelPath: 'name',
                valuePath: 'id',
                defaultOption: function ($el, options) {
                    var performer = this.model.get('performer');

                    if (_.isObject(performer)) {
                        this.model.set({performer: performer.id}, {silent: true});
                        return {
                            label: performer.name,
                            value: performer.id
                        }
                    }

                    return {
                        label: '',
                        value: null
                    }
                }
            }
        }
    },

    behaviors: {
        form: {
            behaviorClass: FormBehavior
        },
        file: {
            behaviorClass: FileBehavior
        }
    },

    onSubmit: function (e) {
        e.preventDefault();

        var problem_id = this.model.get('problem_id');
        var _this = this;

        this.model.unset('draft');

        this.model.save(null, {
            type: 'POST',
            success: function () {
                window.location.href = Routes.problemFeedPath(problem_id);

                Events.trigger('notification:success', {
                    message: 'Запрос на смену исполнителя ' + (_this.model.get('id') ? 'обновлен' : 'создан')
                })
            }
        });
    },

    onClickSaveDraft: function (e) {
        e.preventDefault();

        var problem_id = this.model.get('problem_id');
        var _this = this;

        this.model.set('draft', true);

        this.model.save(null, {
            type: 'POST',
            success: function () {
                window.location.href = Routes.problemFeedPath(problem_id);

                Events.trigger('notification:success', {
                    message: 'Черновик запроса на смену исполнителя ' + (_this.model.get('id') ? 'обновлен' : 'создан')
                })
            }
        });
    }

});

module.exports = ChangePerformerView;