'use strict';

var Mn = require('backbone.marionette'),
    app = require('../../../app'),
    Routes = require('../../../helpers/routes'),
    HeaderLayoutView;

HeaderLayoutView = Mn.View.extend({

    className: 'headerDiv',

    template: require('./tpl/header.pug'),

    initialize: function () {
        this.model = app.dictionaries.profile;
    },

    ui: {
        show: '.dropdown-toggle'
    },

    events: {
      'click @ui.show': 'showDropdown'
    },

    showDropdown: function () {
        this.$el.find('.head-dropdown').toggleClass('active');
    },

    templateContext: function () {
        var _this = this;

        return {
            Routes: Routes,
            getName: function () {
                var firstName = _this.model.attributes.first_name,
                lasttName = _this.model.attributes.last_name[0];
                return firstName + ' ' + lasttName + '.'
            }
        }
    }

});

module.exports = HeaderLayoutView;