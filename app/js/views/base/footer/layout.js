'use strict';

var Mn = require('backbone.marionette'),
    Bb = require('backbone'),
    Events = require('../../../services/events'),
    FooterLayoutView;

FooterLayoutView = Mn.View.extend({

    initialize: function () {
        this.model = new Bb.Model();
        var currentYear = new Date().getFullYear();
        this.model.set('year', currentYear)
        this.listenTo(Events, 'route:change', function (options) {
            this.model.set(options);

        }, this);

        this.listenTo(this.model, 'change:path', function () {
            this.render();
        }, this);
    },

    template: require('./tpl/footer.pug')

});

module.exports = FooterLayoutView;