'use strict';

var Mn = require('backbone.marionette'),
    $ = require('jquery'),
    FormBehavior = require('../../behaviors/form'),
    History = require('../../services/history'),
    Routes = require('../../helpers/routes'),
    SortView;

SortView = Mn.View.extend({

    template: require('./tpl/sort.pug'),

    behaviors: {
        form: {
            behaviorClass: FormBehavior
        }
    },

    events: {
        'click .btn[type="submit"]': 'onSubmitClick',
        'click .control-label': 'onSortItemClick'
    },

    onSortItemClick: function (e) {
        var target = e.target;
        var $input = $(target).parent('.form-group').find('input');

        if ($input.val() == 0) {
            $input.val($input.attr('name'));
            $input.addClass('control-sort-plus');
            $input.removeClass('control-sort-minus');
        } else if ($input.val() == $input.attr('name')) {
            $input.val('-' + $input.attr('name'));
            $input.addClass('control-sort-minus');
            $input.removeClass('control-sort-plus');
        } else if ($input.val() == ('-' + $input.attr('name'))) {
            $input.val($input.attr('name'));
            $input.addClass('control-sort-plus');
            $input.removeClass('control-sort-minus');
        }

        $input.trigger('change');
    },

    onSubmitClick: function (e) {
        e.preventDefault();

        History.navigate(Routes['executor' + this.getOption('type') + 'Path'](this.model.getURLParams()));
    },

    bindings: {
        '[name="problem__expected_answer_dt"]': {
            observe: 'o',
            updateView: true
        },
        '[name="assign_dt"]': {
            observe: 'o',
            updateView: true
        },
        '[name="problem__last_petition__created_at"]': {
            observe: 'o',
            updateView: true
        },
        '[name="priority"]': {
            observe: 'o',
            updateView: true
        },
    }

});

module.exports = SortView;