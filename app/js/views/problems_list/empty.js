'use strict';

var Mn = require('backbone.marionette'),
    ProblemsEmptyView;

ProblemsEmptyView = Mn.View.extend({

    tagName: 'li',
    template: require('./tpl/empty.pug')

});

module.exports = ProblemsEmptyView;