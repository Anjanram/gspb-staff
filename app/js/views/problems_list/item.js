'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    ProblemsItemView;

ProblemsItemView = Mn.View.extend({

    tagName: 'li',
    template: require('./tpl/layout.pug'),

    templateContext: function () {
        var _this = this;

        return {
            getAnswerStart: function () {
                return _this.model.getAnswerStart();
            },
            getAnswerEnd: function () {
                return _this.model.getAnswerEnd();
            },
            getAnswerLast: function () {
                return _this.model.getAnswerLast();
            },
            getDelta: function () {
                return _this.model.getDiff() + ' дн.';
            },
            Routes: Routes
        }
    },

    ui: {
        open: '.show-btn'
    },

    events: {
        'click @ui.open': 'showMore'
    },

    showMore: function () {
        var _this = this;

        function call() {
            var btn = _this.$el.find('i.material-icons.toggle-btn');
            if (btn[0].innerText == 'add') {
                _this.$el.find('i.material-icons.toggle-btn').empty();
                _this.$el.find('i.material-icons.toggle-btn').append('remove')

            } else {
                _this.$el.find('i.material-icons.toggle-btn').empty();
                _this.$el.find('i.material-icons.toggle-btn').append('add')
            }
        }

        this.$el.find('div.full-inform').toggle(call());
    }

});

module.exports = ProblemsItemView;