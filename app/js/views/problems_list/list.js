'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    app = require('../../app'),
    Routes = require('../../helpers/routes'),
    ProblemsItemView = require('./item'),
    ProblemsEmptyView = require('./empty'),
    ProblemsCollectionView;

ProblemsCollectionView = Mn.CollectionView.extend({

    tagName: 'ul',
    childView: ProblemsItemView,
    emptyView: ProblemsEmptyView

});

var LinksView = Mn.View.extend({

    tagName: 'ul',
    className: 'pager',
    template: require('./tpl/links.pug'),

    templateContext: function () {
        var _this = this;

        return {
            isHavePrevious: function () {
                return _this.collection.isHavePrevious();
            },

            isHaveNext: function () {
                return _this.collection.isHaveNext();
            },

            getPrevious: function () {
                if (!_this.collection.isHavePrevious()) {
                    return '#';
                }

                return Routes['executor' + _this.getOption('settings')['type'] + 'Path'](_this.collection.getPrevious().split('?')[1]);
            },

            getNext: function () {
                if (!_this.collection.isHaveNext()) {
                    return '#';
                }

                return Routes['executor' + _this.getOption('settings')['type'] + 'Path'](_this.collection.getNext().split('?')[1]);
            }
        }
    }

});

var ProblemsWithLinksView = Mn.View.extend({

    template: _.template('<div id="problems_list_region"></div><div id="problems_links_region"></div>'),

    regions: {
        problemsListRegion: '#problems_list_region',
        problemsLinksRegion: '#problems_links_region'
    },

    onRender: function () {
        this.showChildView('problemsListRegion', new ProblemsCollectionView({
            collection: this.collection,
            settings: this.getOption('settings')
        }));

        if (this.collection.isHaveLinks()) {
            this.showChildView('problemsLinksRegion', new LinksView({
                collection: this.collection,
                settings: this.getOption('settings')
            }))
        }
    }

});

module.exports = ProblemsWithLinksView;