'use strict';

var Mn = require('backbone.marionette'),
    App = require('../../app'),
    FormBehavior = require('../../behaviors/form'),
    History = require('../../services/history'),
    Routes = require('../../helpers/routes'),
    FilterView;

FilterView = Mn.View.extend({

    template: require('./tpl/filter.pug'),

    behaviors: {
        form: {
            behaviorClass: FormBehavior
        }
    },

    events: {
        'click .btn[type="reset"]': 'onResetClick',
        'click .btn[type="submit"]': 'onSubmitClick'
    },

    onResetClick: function (e) {
        e.preventDefault();

        this.model.unset('status');
        this.model.unset('district');
        this.model.unset('date_from');
        this.model.unset('date_to');

        History.navigate(Routes['executor' + this.getOption('type') + 'Path'](this.model.getURLParams()));

    },

    onSubmitClick: function (e) {
        e.preventDefault();

        History.navigate(Routes['executor' + this.getOption('type') + 'Path'](this.model.getURLParams()));
    },

    bindings: {
        '[name="status"]': {
            observe: 'status',
            updateView: true,
            selectOptions: {
                collection:  App.dictionaries.statuses,
                labelPath: 'name',
                valuePath: 'id',
                defaultOption: {
                    label: '',
                    value: null
                }
            }
        },
        '[name="district"]': {
            observe: 'district',
            updateView: true,
            selectOptions: {
                collection: App.dictionaries.districts,
                labelPath: 'name',
                valuePath: 'id',
                defaultOption: {
                    label: '',
                    value: null
                }
            }
        },
        '[name="date_from"]': {
            observe: 'date_from',
            updateView: true
        },
        '[name="date_to"]': {
            observe: 'date_to',
            updateView: true
        }
    }

});

module.exports = FilterView;