'use strict';

var Mn = require('backbone.marionette'),
    PageView = require('../page/page'),
    RootLayoutView;

RootLayoutView = Mn.View.extend({

    el: 'body',

    regions: {
        pageRegion: '#page_region',
        headerRegion: '#header_region',
        footerRegion: '#footer_region',
        notificationsRegion: '#notifications_region'
    },
    onRender: function () {
      this.showPage();
    },

    showPage: function () {
        this.showChildView('pageRegion', new PageView());
    }

});

module.exports = RootLayoutView;