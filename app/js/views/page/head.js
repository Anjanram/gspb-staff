'use strict';

var Mn = require('backbone.marionette'),
    search = require('./searching'),
    Head;

Head = Mn.View.extend({

    template: require('./tpl/head.pug'),

    templateContext: function () {
        var _this = this;
        return {
            getTitle: function () {
                return _this.settings.title
            },
            getOrgName: function () {
                return _this.settings.org
            }
        }
    },

    regions: {
        searchRegion: '#search_region',
        btnsRegion: '#header_btns_region'
    },

    initialize: function (options) {
        this.settings = options.settings || {}
    },

    onRender: function () {
        this.showSearch(this.settings.searchShow)
    },

    showSearch: function (show) {
        if (show) {
            this.showChildView('searchRegion', new search({
                settings: this.settings,
                model: this.model
            }))
        }
    }

});

module.exports = Head;