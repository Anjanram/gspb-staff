'use strict';

var Mn = require('backbone.marionette'),
    PageHead = require('./head'),
    PageContent = require('./content'),
    PageFooter = require('./footer'),
    Page;

Page = Mn.View.extend({

    template: require('./tpl/layout.pug'),

    regions: {
        pageRegionHeader: '#page_header_region',
        pageRegionContent: '#page_content_region',
        pageRegionFooter: '#page_footer_region'
    },

    settings: {
        title: 'Кабинет исполнителя',
        org: "",
        searchShow: false
    },

    onRender: function () {
        this.showHeader();
        this.showContent();
        this.showFooter();
    },

    showHeader: function () {
        this.showChildView('pageRegionHeader',
            new PageHead({
                settings: this.settings
            })
        );
    },

    showContent: function () {
        this.showChildView('pageRegionContent', new PageContent());
    },

    showFooter: function () {
        this.showChildView('pageRegionFooter', new PageFooter());
    }

});

module.exports = Page;