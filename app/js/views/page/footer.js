'use strict';

var Mn = require('backbone.marionette'),
    Footer;

Footer = Mn.View.extend({

    template: require('./tpl/footer.pug')

});

module.exports = Footer;