'use strict';

var Mn = require('backbone.marionette'),
    Content;

Content = Mn.View.extend({

    template: require('./tpl/content.pug')

});

module.exports = Content;