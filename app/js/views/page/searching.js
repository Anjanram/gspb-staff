'use strict';

var Mn = require('backbone.marionette'),
    FormBehavior = require('../../behaviors/form'),
    History = require('../../services/history'),
    Routes = require('../../helpers/routes'),
    Searching;

Searching = Mn.View.extend({

    template: require('./tpl/searching.pug'),

    templateContext: function () {
        var _this = this;
        return {
            getTitle: function () {
                return _this.settings.titleSearchField
            }
        }
    },

    initialize: function (options) {
        this.settings = options.settings || {}
    },

    events: {
        'submit form': 'onSubmit'
    },

    onSubmit: function (e) {
        e.preventDefault();

        History.navigate(Routes['executor' + this.settings.type + 'Path'](this.model.getURLParams()));
    },

    behaviors: {
        form: {
            behaviorClass: FormBehavior
        }
    },

    bindings: {
        '.page-header__search-field': {
            observe: 'search',
            updateView: true
        }
    }

});

module.exports = Searching;