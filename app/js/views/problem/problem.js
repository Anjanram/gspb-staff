'use strict';

var Mn = require('backbone.marionette'),
    mapView = require('./map'),
    ctrlMyView = require('./my'),
    ctrlOrgView = require('./org'),
    ChangePerformerBtnView = require('./change-performer'),
    Routes = require('../../helpers/routes'),
    ProblemView;

ProblemView = Mn.View.extend({

    template: require('./tpl/layout.pug'),

    templateContext: function () {
        var _this = this;

        return {
            Routes: Routes,
            getAnswerEnd: function () {
                return _this.model.getAnswerEnd();
            },
            getClassPriority: function () {
                var CssClass = '';
                switch (_this.model.getPriority()) {
                    case 1:
                        CssClass = 'low';
                        break;
                    case 3:
                        CssClass =  'high';
                        break;
                }
                return CssClass;
            },
            getDiff: function () {
                return _this.model.getDiff();
            }
        }
    },

    regions: {
        pageRegionMap: '#page_region_problem_map',
        pageRegionCtrl1: '#page_region_ctrl_btn1',
        pageRegionCtrl2: '#page_region_ctrl_btn2',
        pageRegionCtrl3: '#page_region_ctrl_btn3'
    },

    onRender: function () {
        this.showMap();
        this.showControls();
    },

    showMap: function () {
        this.showChildView('pageRegionMap',
            new mapView(this.model.getCoordinates())
        );
    },

    showControls: function () {
        //TODO rewrite

        if (this.model.isCanPerformerAnswerCreate()) {
            this.showChildView('pageRegionCtrl1',
                new ctrlMyView({
                    id: this.model.id
                })
            );
        }

        if (this.model.isCanPerformerRoutesPerformerRefuse()) {
            this.showChildView('pageRegionCtrl2',
                new ChangePerformerBtnView({
                    id: this.model.id
                })
            );
        }

        if (this.model.isCanProblemUserAssign()) {
            this.showChildView('pageRegionCtrl3',
                new ctrlOrgView({
                    id: this.model.id
                })
            );
        }
    }

});

module.exports = ProblemView;