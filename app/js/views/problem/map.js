'use strict';

var Mn = require('backbone.marionette'),
    Map;

Map = Mn.View.extend({

    template: require('./tpl/map.pug'),

    templateContext: function () {
        var _this = this;

        return {
            getMap: function () {
                var req = 'http://static-api.maps.sputnik.ru/v1/?width=288&height=130&z=15&' +
                    'clng=' + _this.getOption('lon') +
                    '&clat=' + _this.getOption('lat') +
                    '&mlng=' + _this.getOption('lon') +
                    '&mlat=' + _this.getOption('lat');

                return req
            }
        }
    }

});

module.exports = Map;