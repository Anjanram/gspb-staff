'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    $ = require('jquery'),
    History = require('../../services/history'),
    Events = require('../../services/events'),
    gspb = require('../../services/session'),
    org;

org = Mn.View.extend({

    template: require('./tpl/org.pug'),

    templateContext: function () {
        var _this = this;
        return {
            getId: function () {
                return _this.id
            },
            Routes: Routes
        }
    },

    ui: {
        userAssign: '.js-user-assign'
    },

    events: {
        'click @ui.userAssign': 'userAssign'
    },

    userAssign: function () {
        var _this = this;
        $.ajax({
            type: "POST",
            url: '/staff/api/v2/problems/' + _this.id + '/user_assign/',
            headers: {
                'X-CSRFToken': gspb.getCSRFToken()
            },
            success: function () {
                Events.trigger('notification:success', {
                    message: 'Сообщение взято в работу'
                });

                History.reload();
            }
        })
    }

});

module.exports = org;
