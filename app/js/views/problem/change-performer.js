'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    ChangePerformerBtnView;

ChangePerformerBtnView = Mn.View.extend({

    template: require('./tpl/change-performer.pug'),

    templateContext: function () {
        var _this = this;

        return {
            getId: function () {
              return _this.id
            },
            Routes: Routes
        }
    }

});

module.exports = ChangePerformerBtnView;