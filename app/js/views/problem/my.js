'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    my;

my = Mn.View.extend({

    template: require('./tpl/my.pug'),

    templateContext: function () {
        var _this = this;

        return {
            getId: function () {
              return _this.id
            },
            Routes: Routes
        }
    }

});

module.exports = my;