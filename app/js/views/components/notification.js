'use strict';

var Notification = require('marionette-notifications'),
    Events = require('../../services/events');

module.exports = Notification.CollectionView.extend({

    initialize: function () {
        // Notification.prototype.initialize.call(this);

        this.listenTo(Events, 'notification:info', this.onInfo);
        this.listenTo(Events, 'notification:error', this.onError);
        this.listenTo(Events, 'notification:success', this.onSuccess);
    },

    onError: function (options) {
        this.error({
            content: options.message || 'Неизвестная ошибка'
        })
    },

    onInfo: function (options) {
        this.info({
            content: options.message || 'Неизвестное сообщение'
        });
    },

    onSuccess: function (options) {
        this.success({
            content: options.message || 'Неизвестное сообщение'
        });
    }

});