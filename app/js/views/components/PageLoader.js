'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    PageLoader;

PageLoader = Mn.View.extend({
    className: 'page-loader',
    template: _.template('<img src="images/page_loader.svg">')

});

module.exports = PageLoader;