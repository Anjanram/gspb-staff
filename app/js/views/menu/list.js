'use strict';

var Mn = require('backbone.marionette'),
    MenuItemView = require('./item'),
    MenuCollectionView;

MenuCollectionView = Mn.CollectionView.extend({

    tagName: 'ul',
    childView: MenuItemView,

});

module.exports = MenuCollectionView;