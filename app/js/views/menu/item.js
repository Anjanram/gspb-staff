'use strict';

var Mn = require('backbone.marionette'),
    Routes = require('../../helpers/routes'),
    MenuItemView;

MenuItemView = Mn.View.extend({

    tagName: 'li',

    template: require('./tpl/item.pug'),

    templateContext: function () {
        // console.log(this);
        return {
            Routes: Routes
        }
    }

});

module.exports = MenuItemView;