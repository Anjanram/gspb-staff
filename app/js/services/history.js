'use strict';

var Bb = require('backbone'),
    history = Bb.history;

var History = {

    start: function () {
        history.start({
            root: '/'
        })
    },

    reload: function () {
        history.loadUrl(history.fragment);
    },

    navigate: function (route, options) {
        history.navigate(route, options);
    },

    redirectToLogin: function () {
        window.location = '/m/#/accounts/login/';
    },

    fragment: function () {
        return history.getFragment();
    }

};

module.exports = History;