'use strict';

var Bb = require('backbone'),
    _ = require('underscore'),
    Events;

Events = _.extend({}, Bb.Events);

module.exports = Events;