'use strict';

var Mn = require('backbone.marionette'),
    PageView;

PageView = Mn.View.extend({
    template: require('./base/main.pug'),
    regions: {
        pageRegionHeader: '#page_header_region',
        pageRegionContent: '#page_content_region',
        pageRegionFooter: '#page_footer_region'
    }
});

module.exports = PageView;