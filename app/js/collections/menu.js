'use strict';

var BaseCollection = require('./base'),
    _ = require('underscore'),
    Routes = require('../helpers/routes'),
    MenuModel = require('../models/menu'),
    MenuCollection;

MenuCollection = BaseCollection.extend({

    url: '/staff/menu/',

    model: MenuModel,

    getSubmenus: function () {
        return _.flatten(this.map(function (item) {
            return item.get('submenus')
        }))
    },

    isContainsMenu: function (group, title) {
        var group = this.find({group: group});

        if (!!group) {
            var submenus = _.findWhere(group.get('submenus'), {title: title});

            if (submenus) {
                return true;
            }

            return false;
        }

        return false;
    },

    parse: function (res) {
        var groups = [];

        _.each(res.menu, function (item) {
            if ((item.title === 'Все проблемы') && (item.group === '')) {
                return;
            }

            switch (item.url) {
                case "/api/v3.1/staff/executor/answer_preparing/my/":
                    item.link = Routes.executorMyPath('o=problem__expected_answer_dt');
                    break;
                case "/api/v3.1/staff/executor/answer_preparing/organization/":
                    item.link = Routes.executorOrganizationPath('o=problem__expected_answer_dt');
                    break;
                case "/api/v3.1/staff/executor/answer_preparing/department/":
                    item.link = Routes.executorDepartmentPath('o=problem__expected_answer_dt');
                    break;
                case "/api/v3.1/staff/executor/answer_drafts/":
                    item.link = Routes.executorDraftPath('o=problem__expected_answer_dt');
                    break;
                case "/api/v3.1/staff/executor/performer_change_drafts/":
                    item.link = Routes.executorDraftChangePath('o=problem__expected_answer_dt');
                    break;
            }

            var group = _.find(groups, function (_group) {
                return _group.group == item.group;
            });

            if (group) {
                group.submenus.push(_.omit(item,'group'))
            } else {
                groups.push({
                    group: item.group,
                    submenus: [_.omit(item,'group')]
                })
            }
        });

        return groups;
    }

});

module.exports = MenuCollection;
