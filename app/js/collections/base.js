'use strict';

var Bb = require('backbone'),
    _ = require('underscore'),
    AppConfig = require('../config'),
    History = require('../services/history'),
    BaseCollection;

BaseCollection = Bb.Collection.extend({

    initialize: function() {
        this.url = AppConfig.apiPath + _.result(this, 'url');

        this.listenTo(this, 'error', function (model, response, options) {
            if (response.status == 401) {
                History.redirectToLogin();
            }
        })
    }

});

module.exports = BaseCollection;
