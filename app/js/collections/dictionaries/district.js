'use strict';

var BaseCollection = require('./../base'),
    DistrictsCollection;

DistrictsCollection = BaseCollection.extend({

    url : '/districts/',

});

module.exports = DistrictsCollection;