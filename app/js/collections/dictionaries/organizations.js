'use strict';

var BaseCollection = require('./../base'),
    OrgsModel = require('../../models/base'),
    OrganizationsCollection;

OrganizationsCollection = BaseCollection.extend({
    url : '/staff/organizations/',
    model: OrgsModel
});

module.exports = OrganizationsCollection;