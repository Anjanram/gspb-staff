'use strict';

var BaseCollection = require('./../base'),
    StatusesCollection;

StatusesCollection = BaseCollection.extend({

    url : '/statuses/',

});

module.exports = StatusesCollection;