'use strict';

var BaseProblemsCollection = require('./BaseProblemsCollection'),
    DraftsProblems;

DraftsProblems = BaseProblemsCollection.extend({

    url: '/staff/executor/answer_drafts/',

});

module.exports = DraftsProblems;