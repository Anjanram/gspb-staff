'use strict';

var BaseProblemsCollection = require('./BaseProblemsCollection'),
    OrganazaionProblems;

OrganazaionProblems = BaseProblemsCollection.extend({

    url: '/staff/executor/answer_preparing/organization/',

});

module.exports = OrganazaionProblems;