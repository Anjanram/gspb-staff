'use strict';

var BaseProblemsCollection = require('./BaseProblemsCollection'),
    DepartmentProblems;

DepartmentProblems = BaseProblemsCollection.extend({

    url: '/staff/executor/answer_preparing/department/',

});

module.exports = DepartmentProblems;