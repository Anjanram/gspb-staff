'use strict';

var BaseProblemsCollection = require('./BaseProblemsCollection'),
    ChangeDraftsProblems;

ChangeDraftsProblems = BaseProblemsCollection.extend({

    url: '/staff/executor/performer_change_drafts/',

});

module.exports = ChangeDraftsProblems;