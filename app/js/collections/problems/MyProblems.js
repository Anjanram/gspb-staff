'use strict';

var BaseProblemsCollection = require('./BaseProblemsCollection'),
    MyProblems;

MyProblems = BaseProblemsCollection.extend({

    url: '/staff/executor/answer_preparing/my/',

});

module.exports = MyProblems;