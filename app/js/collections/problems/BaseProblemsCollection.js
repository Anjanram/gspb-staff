'use strict';

var BaseCollection = require('../base'),
    BaseProblemsModel = require('../../models/problems/BaseProblems'),
    BaseProblemsCollection;

BaseProblemsCollection = BaseCollection.extend({

    model: BaseProblemsModel,

    parse: function (res) {
        this.previous = res.previous;
        this.next = res.next;
        this.count = res.count;
        return res.results;
    },

    isHavePrevious: function () {
        return !!this.previous;
    },

    isHaveNext: function () {
        return !!this.next;
    },

    isHaveLinks: function () {
        return this.isHavePrevious() || this.isHaveNext();
    },

    getPrevious: function () {
        return this.previous;
    },

    getNext: function () {
        return this.next;
    },

    getCount: function () {
        return this.count;
    }

});

module.exports = BaseProblemsCollection;