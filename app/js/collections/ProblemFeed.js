'use strict';

var BaseCollection = require('./base'),
    AppConfig = require('../config'),
    BaseFeed = require('../models/feed/BaseFeed'),
    BasePetition = require('../models/feed/BasePetition'),
    FeedPetitionRejected = require('../models/feed/FeedPetitionRejected'),
    BaseStatus = require('../models/feed/BaseStatus'),
    BaseRoute = require('../models/feed/BaseRoute'),
    FeedRoutePerformerAssignment = require('../models/feed/FeedRoutePerformerAssignment'),
    FeedRoutePerformerAssignmentCancel = require('../models/feed/FeedRoutePerformerAssignmentCancel'),
    FeedRoutePerformerChangeRevoked = require('../models/feed/FeedRoutePerformerChangeRevoked'),
    FeedRoutePerformerChangeCanRevoke = require('../models/feed/FeedRoutePerformerChangeCanRevoke'),
    FeedRoutePerformerChangeDraft = require('../models/feed/FeedRoutePerformerChangeDraft'),
    BaseAnswer = require('../models/feed/BaseAnswer'),
    FeedAnswerRevoked = require('../models/feed/FeedAnswerRevoked'),
    FeedAnswerRejected = require('../models/feed/FeedAnswerRejected'),
    FeedAnswerDraft = require('../models/feed/FeedAnswerDraft'),
    FeedAnswerCanRevoke = require('../models/feed/FeedAnswerCanRevoke'),
    FeedExecutionDateChange = require('../models/feed/FeedExecutionDateChange'),
    ProblemFeed;

ProblemFeed = BaseCollection.extend({

    initialize: function (models, options) {
        this.id = options.id;
    },

    url: function () {
        return AppConfig.apiPath + '/staff/problems/' + this.id;
    },

    model: function (attr, options) {
        //TODO сделать общую логику определения с вьюхой
        var feed_type = attr.widget;

        var model;

        switch (feed_type) {
            case 'petition':
            case 'petition_moderation':
                model = new BasePetition(attr, options);
                break;

            case 'petition_rejected':
                model = new FeedPetitionRejected(attr, options);
                break;

            case 'status':
                model = new BaseStatus(attr, options);
                break;

            case 'route':
            case 'route_rejected':
                model = new BaseRoute(attr, options);
                break;

            case 'route_performer_assignment':
                model = new FeedRoutePerformerAssignment(attr, options);
                break;

            case 'route_performer_assignment_cancel':
                model = new FeedRoutePerformerAssignmentCancel(attr, options);
                break;

            case 'route_performer_change_revoked':
            case 'route_performer_change_rejected':
                model = new FeedRoutePerformerChangeRevoked(attr, options);
                break;

            case 'route_performer_change_can_revoke':
                model = new FeedRoutePerformerChangeCanRevoke(attr, options);
                break;

            case 'route_performer_change_draft':
                model = new FeedRoutePerformerChangeDraft(attr, options);
                break;

            case 'execution_date_change':
                model = new FeedExecutionDateChange(attr, options);
                break;

            case 'answer':
            case 'answer_coordinator_accepted':
            case 'answer_revoked':
            case 'answer_rejected':
                model = new BaseAnswer(attr, options);
                break;

            case 'answer_can_revoke':
                model = new FeedAnswerCanRevoke(attr, options);
                break;

            case 'answer_draft':
                model = new FeedAnswerDraft(attr, options);
                break;

            default:
                model = new BaseFeed(attr, options);
        }

        // console.log(model);

        return model;
    },

    parse: function (resp) {
        return resp.feed;
    }

});

module.exports = ProblemFeed;