'use strict';

var Mn = require('backbone.marionette'),
    App = require('../app'),
    Header = require('../views/base/header/layout'),
    ProfileModel = require('../models/profile'),
    HeaderController;

HeaderController = Mn.Object.extend({

    initialize: function () {
        var profile = new ProfileModel();

        App.layout.getRegion('headerRegion').show(
            new Header({
                  model: profile
            })
        );
        profile.fetch()
    }

});

module.exports = HeaderController;