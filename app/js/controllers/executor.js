'use strict';

var Mn = require('backbone.marionette'),
    $ = require('jquery'),
    _ = require('underscore'),
    App = require('../app'),
    Menu = require('../views/pages/menu'),
    BaseProblemsPage = require('../views/pages/problems/BaseProblemsPage'),
    BaseProblemsFilterPage = require('../views/pages/problems/BaseProblemsFilterPage'),
    BaseProblemsSortPage = require('../views/pages/problems/BaseProblemsSortPage'),
    MenuCollection = require('../collections/menu'),
    BaseProblemsMenuCollection = require('../collections/problems/BaseProblemsMenuCollection'),
    MyProblemsCollection = require('../collections/problems/MyProblems'),
    OrganizationProblemsCollection = require('../collections/problems/OrganazaionProblems'),
    DepartmentProblemsCollection = require('../collections/problems/DepartmentProblems'),
    DraftProblemsCollection = require('../collections/problems/DraftsProblems'),
    ChangeDraftProblemsCollection = require('../collections/problems/ChangeDraftsProblems'),
    ProblemsConfigModel = require('../models/ProblemsConfig'),
    ExecutorController;

ExecutorController = Mn.Object.extend({

    index: function() {
        console.log('routes:executor:index');

        var config = new ProblemsConfigModel();

        var menu = new MenuCollection();

        this.listenTo(menu, 'sync', function () {
            var submenus = menu.getSubmenus();
            var submenus_collections = [];

            _.each(submenus, function (item) {
                var col = new BaseProblemsMenuCollection();

                col.url = item.url;
                col.submenu = item;

                submenus_collections.push(col)
            });

            // var submenus_deferreds = _.map(submenus_collections, function (item) {
            //     return item.fetch({
            //         data: config.toJSON(),
            //         success: function(model, response, options) {
            //             // model.submenu.indicator = response.count;
            //         }
            //     })
            // });

            // $.when.apply($, submenus_deferreds).then(function () {
                App.layout.showChildView('pageRegion',
                    new Menu({
                        collection: menu
                    })
                );
            // });
        });

        menu.fetch();
    },

    _showProblemList: function (query, options) {
        var collection = options.collection;
        var config = new ProblemsConfigModel(query, {parse: true});

        this.listenTo(collection, 'sync', function () {
            App.layout.showChildView('pageRegion',
                new BaseProblemsPage({
                    collection: collection,
                    model: config,
                    title: options.title,
                    type: options.type,
                    titleSearchField: options.titleSearchField || ''
                })
            )
        });

        collection.fetch({data: config.toJSON()});
    },

    _showFilter: function (query, options) {
        var config = new ProblemsConfigModel(query, {parse: true});

        App.layout.showChildView('pageRegion',
            new BaseProblemsFilterPage({
                model: config,
                type: options.type,
                title: options.title
            })
        );
    },

    _showSort: function (query, options) {
        var config = new ProblemsConfigModel(query, {parse: true});

        App.layout.showChildView('pageRegion',
            new BaseProblemsSortPage({
                model: config,
                type: options.type,
                title: options.title
            })
        );
    },

    /*
    *
    * Мои сообщения
    *
    * */

    my: function (query) {
        var options = {
            title: 'Мои',
            type: 'My',
            titleSearchField: 'Подготовить ответ',
            collection: new MyProblemsCollection()
        };

        this._showProblemList(query, options);
    },

    myFilter: function (query) {
        var options = {
            title: 'Мои',
            type: 'My',
        };

        this._showFilter(query, options);
    },

    mySort: function (query) {
        var options = {
            title: 'Мои',
            type: 'My',
        };

        this._showSort(query, options);
    },

    /*
    *
    * Сообщения организации
    *
    * */

    organization: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Organization',
            titleSearchField: 'Подготовить ответ',
            collection: new OrganizationProblemsCollection()
        };

        this._showProblemList(query, options);
    },

    organizationFilter: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Organization'
        };

        this._showFilter(query, options);
    },

    organizationSort: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Organization'
        };

        this._showSort(query, options);
    },

    /*
     *
     * Сообщения организации
     *
     * */

    department: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Department',
            titleSearchField: 'Подготовить ответ',
            collection: new DepartmentProblemsCollection()
        };

        this._showProblemList(query, options);
    },

    departmentFilter: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Department'
        };

        this._showFilter(query, options);
    },

    departmentSort: function (query) {
        var options = {
            title: 'Нераспределенные',
            type: 'Department'
        };

        this._showSort(query, options);
    },

    /*
    *
    * Черновики ответов
    *
    * */

    draft: function (query) {
        var options = {
            title: 'Черновики',
            type: 'Draft',
            titleSearchField: 'Ответы',
            collection: new DraftProblemsCollection()
        };

        this._showProblemList(query, options);
    },

    draftFilter: function (query) {
        var options = {
            title: 'Черновики',
            type: 'Draft',
        };

        this._showFilter(query, options);
    },

    draftSort: function (query) {
        var options = {
            title: 'Черновики',
            type: 'Draft',
        };

        this._showSort(query, options);
    },

    /*
    *
    * Черновики запросов на смену исполнителей
    *
    * */

    draftChange: function (query) {
        var options = {
            title: 'Черновики',
            type: 'DraftChange',
            titleSearchField: 'Запросы на смену',
            collection: new ChangeDraftProblemsCollection()
        };

        // this._showProblemList(query, options);
    },

    draftChangeFilter: function (query) {
        var options = {
            title: 'Черновики',
            type: 'DraftChange',
        };

        this._showFilter(query, options);
    },

    draftChangeSort: function (query) {
        var options = {
            title: 'Черновики',
            type: 'DraftChange',
        };

        this._showSort(query, options);l
    }

});

module.exports = ExecutorController;