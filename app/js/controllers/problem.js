'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    App = require('../app'),
    Problem = require('../views/pages/problem'),
    AnswerPageView = require('../views/pages/answers'),
    PerformerChangePageView = require('../views/pages/change_performer'),
    ProblemModel = require('../models/problems'),
    AnswersModel = require('../models/answers'),
    AnswersUpdateModel = require('../models/answersUpdate'),
    ChangePerformerModel = require('../models/change_performer'),
    ChangePerformerUpdateModel = require('../models/change_performerUpdate'),
    FeedUserPageView = require('../views/pages/problemUserFeed'),
    FeedPageView = require('../views/pages/problemFeed'),
    FeedCollection = require('../collections/ProblemFeed'),
    ProblemController;

ProblemController = Mn.Object.extend({

    index: function (id) {
        console.info('routes:problems:show');
        var problem = new ProblemModel({
            id: id
        });

        this.listenTo(problem, 'sync', function () {
            App.layout.showChildView('pageRegion',
            new Problem({
                model: problem
            }))
        });

        problem.fetch();
    },

    feed: function (id) {
        console.info('routes:problems:feed');
        var feed = new FeedCollection(null, {id:id});

        this.listenTo(feed, 'sync', function () {
            App.layout.showChildView('pageRegion',
                new FeedPageView({
                    collection: feed
                })
            );
        });

        feed.fetch();
    },

    feedUser: function (id) {
        console.info('routes:problems:feed:user');
        var feed = new FeedCollection(null, {id:id});

        this.listenTo(feed, 'sync', function () {
            App.layout.showChildView('pageRegion',
                new FeedUserPageView({
                    collection: feed
                })
            );
        });

        feed.fetch();
    },

    answer: function (id) {
        var answer = new AnswersModel({
            problem_id: id
        });

        App.layout.showChildView('pageRegion',
            new AnswerPageView({
                model: answer
            })
        );
    },

    answerEdit: function (id, msg_id) {
        var feed = new FeedCollection(null, {id: id});

        this.listenTo(feed, 'sync', function () {

            //TODO Вынести получение фида по ид и сборку в коллекцию
            var answerFromFeed = feed.find(function (model) {
                return model.id == msg_id;
            });

            if (answerFromFeed) {
                var answer = new AnswersUpdateModel(_.extend(answerFromFeed.get('payload'), {problem_id: id}), {parse: true});

                App.layout.showChildView('pageRegion',
                    new AnswerPageView({
                        model: answer
                    })
                );
            }
        });

        feed.fetch();
    },

    performerChange: function (id) {
        console.info('routes:problems:performer_change');

        var performer_change = new ChangePerformerModel({
            problem_id: id
        });

        App.layout.showChildView('pageRegion',
            new PerformerChangePageView({
                model: performer_change
            })
        );
    },

    performerChangeEdit: function (id, msg_id) {
        var feed = new FeedCollection(null, {id: id});

        this.listenTo(feed, 'sync', function () {

            //TODO Вынести получение фида по ид и сборку в коллекцию
            var performer_changeFromFeed = feed.find(function (model) {
                return model.id == msg_id;
            });

            if (performer_changeFromFeed) {
                var performer_change = new ChangePerformerUpdateModel(_.extend(performer_changeFromFeed.get('payload'), {problem_id: id}), {parse: true});

                App.layout.showChildView('pageRegion',
                    new PerformerChangePageView({
                        model: performer_change
                    })
                );
            }
        });

        feed.fetch();
    }
});

module.exports = ProblemController;