'use strict';

var Mn = require('backbone.marionette'),
    App = require('../app'),
    Footer = require('../views/base/footer/layout'),
    FooterController;

FooterController = Mn.Object.extend({

    initialize: function () {
        App.layout.getRegion('footerRegion').show(
            new Footer()
        );
    }

});

module.exports = FooterController;