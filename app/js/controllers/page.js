var Mn = require('backbone.marionette'),
    App = require('../app'),
    Page = require('../templates/template'),
    PageController;

PageController = Mn.View.extend({

    initialize: function () {
        App.layout.getRegion('pageRegion').show(
            new Page()
        )
    }

});

module.exports = PageController;