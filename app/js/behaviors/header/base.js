'use strict';

var Mn = require('backbone.marionette'),
    Bn = require('backbone'),
    _ = require('underscore'),
    BtnsView = require('./btn/BaseBtns'),
    BaseBehavior;

BaseBehavior = Mn.Behavior.extend({

    getRegion: function () {
        return 'btnsRegion'
    },

    onRender: function () {
        this.view.showChildView(this.getRegion(), new BtnsView({
            collection: this.options.collection
        }));
    }

});

module.exports = BaseBehavior;