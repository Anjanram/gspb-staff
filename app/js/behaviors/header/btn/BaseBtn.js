'use strict';

var Mn = require('backbone.marionette'),
    _ = require('underscore'),
    BaseBtn;

BaseBtn = Mn.View.extend({

    className: 'header-btn',

    template: require('./tpl/btn.pug'),

    templateContext: function () {
        var _this = this;

        return {
            isActive: function () {
                return !_.isUndefined(_this.model.get('active')) && _this.model.get('active');
            }
        }
    }

});

module.exports = BaseBtn;