'use strict';

var Mn = require('backbone.marionette'),
    BaseBtnView = require('./BaseBtn'),
    BaseBtns;

BaseBtns = Mn.CollectionView.extend({

    className: 'header-btns',

    childView: BaseBtnView

});

module.exports = BaseBtns;