'use strict';

var Mn = require('backbone.marionette'),
    Bb = require('backbone'),
    FilesBehavior;

var FileItemView = Mn.View.extend({

    tagName: 'li',
    className: 'gallery-files__item label label-file',

    template: require('./file-item.pug'),

});

var FileCollectionView = Mn.CollectionView.extend({

    tagName: 'ul',
    className: 'gallery-files',

    childView: FileItemView

});

FilesBehavior = Mn.Behavior.extend({

    onAttach: function () {
        if (this.view.model.isHaveFiles()) {
            var col = new Bb.Collection(this.view.model.getFiles());

            this.view.showChildView('filesRegion', new FileCollectionView({
                collection: col
            }));
        }
    }

});

module.exports = FilesBehavior;