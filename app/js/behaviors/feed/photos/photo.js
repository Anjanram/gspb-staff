'use strict';

var Mn = require('backbone.marionette'),
    Bb = require('backbone'),
    $ = require('jquery'),
    PhotoSwipe = require('photoswipe'),
    PhotoSwipeUI_default = require('photoswipe/dist/photoswipe-ui-default'),
    PhotoBehavior;

var FileItemView = Mn.View.extend({

    tagName: 'li',
    className: 'gallery__item',

    template: require('./photo-item.pug'),

    triggers: {
        'click .gallery__item__link': 'photo:click'
    }

});

var FileCollectionView = Mn.CollectionView.extend({

    tagName: 'ul',
    className: 'gallery',
    childView: FileItemView,

    childViewEvents: {
        'photo:click': 'openPhotoSwipe'
    },

    openPhotoSwipe: function (childView, e) {
        var pswpElement = $('.pswp')[0];

        var slides = this.collection.map(function (model) {
            var url = model.get('url'),
                thumbnail = model.get('thumbnail'),
                size = model.get('size');

            var photoSize = size && size.split('x') || [300, 300];

            return {
                src: url,
                msrc: thumbnail,
                w: parseInt(photoSize[0], 10),
                h: parseInt(photoSize[1], 10)
            };
        });

        var options = {
            index: this.collection.indexOf(childView.model),
            history: false,
            showHideOpacity: true,
            closeOnScroll: false,
            shareButtons: [{
                id: 'download',
                label: 'Скачать',
                url: '{{raw_image_url}}',
                download: true
            }],
            errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">Изображение</a> не может быть загружено.</div>'
        };

        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_default, slides, options);
        gallery.init();
    }

});

PhotoBehavior = Mn.Behavior.extend({

    onAttach: function () {
        if (this.view.model.isHaveImages()) {
            var col = new Bb.Collection(this.view.model.getImages());

            this.view.showChildView('photosRegion', new FileCollectionView({
                collection: col
            }));
        }
    }

});

module.exports = PhotoBehavior;