'use strict';

var Bb = require('backbone'),
    _ = require('underscore'),
    $ = require('jquery');

Bb.$ = $;
window.jQuery = $;
window._ = _;

require('./initializers/stickit');
require('./initializers/events');
require('./initializers/notification');

require('./initializers/dictionaries');

require('./initializers/executor');
require('./initializers/problem');
require('./initializers/footer');
require('./initializers/header');
require('./initializers/page');

require('bootstrap-sass');